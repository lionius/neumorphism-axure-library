# Neumorphism Axure Library

#### 介绍
基于时下比较流行的新拟态风格做的Axure组件库。

#### 预览

项目预览地址：https://sv1lhg.axshare.com

#### Axure版本

制作时使用 Mac Axure 9.0.0.3723，不支持Axure 8 版本。

#### 联系方式

如果想与我取得联系，请邮件我：bosenger@qq.com

也可访问我的博客留言：http://www.baozipm.com

如果同志们确实很喜欢这个组件库，可以捐赠￥6.6支持工作，谢谢大家。

![捐赠二维码](http://baozipmimg.oss-cn-beijing.aliyuncs.com/img/IMG_1274.JPG "捐赠二维码")
